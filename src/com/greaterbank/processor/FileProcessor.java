package com.greaterbank.processor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import org.apache.log4j.Logger;

/**
* The FileProcessor program processes input csv files from the input directory and
* generates a report containing the total credits, total debits, skipped lines
* 
* @author  Gopal Desikan
* @version 1.0
* @since   2017-10-08
*/
public class FileProcessor {
	final static Logger logger = Logger.getLogger(FileProcessor.class);
	private static final String PENDING_DIR = "pending";
	private static final String PROCESSED_DIR = "processed";
	private static final String REPORTS_DIR = "reports";
	private static final String INPUT_FILE_TYPE = ".csv";
	private static final String COMMA = ",";
	static int corruptLines = 0;
	static int processedLines = 0;
	static BigDecimal totalCredits = new BigDecimal(0.00);
	static BigDecimal totalDebits = new BigDecimal(0.00);
	
	/**  
	  * This is the main method that has to be run to start file processing  
	  * @param args[] A variable of type String Array.
	  */
	public static void main(String args[]){
		logger.debug("Starting..................");
		File folder = new File(System.getenv("TRANSACTION_PROCESSING")+"\\"+PENDING_DIR);
		logger.debug("Input folder is: "+folder);
		for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	        	logger.debug("This is a sub-directory..Hence ignoring");
	        } else {
	            if(fileEntry.getName().contains(INPUT_FILE_TYPE)){
	            	logger.debug("The complete path of the input file is: "+System.getenv("TRANSACTION_PROCESSING")+"\\"+PENDING_DIR+"\\"+fileEntry.getName());
	            	processInputFile(System.getenv("TRANSACTION_PROCESSING")+"\\"+PENDING_DIR+"\\"+fileEntry.getName());
	            	logger.debug("Corrupted Records: "+corruptLines);
	        		logger.debug("Total Records Processed: "+processedLines);
	        		logger.debug("Total Debits: "+totalDebits);
	        		logger.debug("Total Credits: "+totalCredits);
	        		writeReport(fileEntry.getName());
	        		moveFile(fileEntry);
	            }
	        }
	    }
	}
	
	/**  
	  * Processes the input file.  
	  * @param fileAddress A variable of type String.
	  */
	private static void processInputFile(String fileAddress){
		try (FileReader fr = new FileReader(fileAddress);
				Scanner sc = new Scanner(fr)) {
					sc.nextLine();
					while (sc.hasNextLine()) {
						String nextLine = sc.nextLine();
						processRecord(nextLine);
						processedLines++;
					}
		} catch (IOException e) {
			logger.error("IOException occurred "+e.getMessage());
		}
	}
	
	/**  
	  * Processes one record in the input file.  
	  * @param record A variable of type String.
	  */
	private static void processRecord(String record){
		if (record != null){
			String[] parts = record.split(COMMA);
			if ((!isNumeric(parts[0])) || (!isNumeric(parts[1]))){
				corruptLines++;
			} else{
				BigDecimal amount = new BigDecimal(parts[1]);
				if (amount.compareTo(BigDecimal.ZERO) > 0){
					totalCredits = totalCredits.add(amount);
				}else if (amount.compareTo(BigDecimal.ZERO) < 0){
					totalDebits = totalDebits.add(amount);
				}
			}
		}
	}
	
	/**  
	  * Checks if the given string is numeric or not.  
	  * @param str A variable of type String.
	  * @return boolean data type. 
	  */
	private static boolean isNumeric(String str){  
	  try{  
	     Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe){  
	    return false;  
	  }  
	  return true;  
	}
	
	/**  
	  * Writes the report in $TRANSACTION_PROCESSING/reports directory
	  * @param inputFileName A variable of type String.
	  * 
	  */
	private static void writeReport(String inputFileName){
		String firstLine = "File Processed: "+inputFileName;
		String secondLine = "Total Accounts: "+processedLines;
		String thirdLine = "Total Credits: $"+totalCredits;
		String fourthLine = "Total Debits: $"+totalDebits.abs();
		String fifthLine = "Skipped Transactions: "+corruptLines;
		Date today = new Date() ;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss") ;
		File file =new File(System.getenv("TRANSACTION_PROCESSING")+"\\"+REPORTS_DIR+"\\"+"finance_customer_transactions_report"+dateFormat.format(today)+".txt");
		if(!file.exists()){
	    	   try {
				file.createNewFile();
			} catch (IOException e) {
				logger.error("IO exception occurred "+e.getMessage());
			}
	    	}
		try(FileWriter fw = new FileWriter(file,true);
		    	BufferedWriter bw = new BufferedWriter(fw)){
			
			bw.write(firstLine);
			bw.write("\r\n");
			bw.write(secondLine);
			bw.write("\r\n");
			bw.write(thirdLine);
			bw.write("\r\n");
			bw.write(fourthLine);
			bw.write("\r\n");
			bw.write(fifthLine);
		} catch (IOException e) {
			logger.error("IO exception occurred "+e.getMessage());
		}
	}
	
	/**  
	  * Moves a given file to $TRANSACTION_PROCESSING/processed directory
	  * @param file A variable of type File.
	  * 
	  */
	private static void moveFile(File file){
		if (file != null){
			file.renameTo(new File(System.getenv("TRANSACTION_PROCESSING")+"\\"+PROCESSED_DIR+"\\"+file.getName()));
			logger.debug("Moved the file "+file.getName()+" to "+System.getenv("TRANSACTION_PROCESSING")+"\\"+PROCESSED_DIR+"\\"+file.getName());
		}
	}
	
}
