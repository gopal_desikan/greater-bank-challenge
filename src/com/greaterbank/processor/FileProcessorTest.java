package com.greaterbank.processor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

@RunWith(BlockJUnit4ClassRunner.class)
public class FileProcessorTest {
    
	private Method isNumericMethod, processRecordMethod, processInputFileMethod, writeReportMethod, moveFileMethod;
	private Class[] parameterTypes = {String.class};
	private Class[] parameterTypesFile = {File.class};
	FileProcessor fp;
	private static final String COMMA = ",";
	Field corruptLines, processedLines, totalCredits, totalDebits;
	
	@Before
	public void setup() throws Exception{
	    fp = new FileProcessor();
		isNumericMethod = fp.getClass().getDeclaredMethod("isNumeric", parameterTypes);
		processRecordMethod = fp.getClass().getDeclaredMethod("processRecord", parameterTypes);
		processInputFileMethod = fp.getClass().getDeclaredMethod("processInputFile", parameterTypes);
		writeReportMethod = fp.getClass().getDeclaredMethod("writeReport", parameterTypes);
		moveFileMethod = fp.getClass().getDeclaredMethod("moveFile", parameterTypesFile);
		writeReportMethod.setAccessible(true);
		isNumericMethod.setAccessible(true);
		moveFileMethod.setAccessible(true);
		processRecordMethod.setAccessible(true);
		processInputFileMethod.setAccessible(true);
		corruptLines = fp.getClass().getDeclaredField("corruptLines");
		processedLines = fp.getClass().getDeclaredField("processedLines");
		totalCredits = fp.getClass().getDeclaredField("totalCredits");
		totalDebits = fp.getClass().getDeclaredField("totalDebits");
		corruptLines.setAccessible(true);
		processedLines.setAccessible(true);
		totalCredits.setAccessible(true);
		totalDebits.setAccessible(true);
		corruptLines.set(this, 0);
		processedLines.set(this, 0);
		totalCredits.set(this, new BigDecimal(0.00));
		totalDebits.set(this, new BigDecimal(0.00));
	}
	@Test
	public void testIsNumericForNumber() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String arg = "23909119";
		boolean result = (boolean) isNumericMethod.invoke(fp, arg);
		assertTrue(result);
	}
	
	@Test
	public void testIsNumericForAlphaNumericString() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		String arg = "23909119AO09";
		boolean result = (boolean) isNumericMethod.invoke(fp, arg);
		assertFalse(result);
	}
	
	@Test
	public void testProcessRecord1() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		String input = "200001829,-3088.25";
		processRecordMethod.invoke(fp, input);
		assertEquals(new BigDecimal(-3088.25), fp.totalDebits);
		assertEquals(new BigDecimal(0.00), fp.totalCredits);
		assertEquals(0, fp.corruptLines);
		
	}
	
	@Test
	public void testProcessRecord2() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		String input = "200001829,30.25";
		processRecordMethod.invoke(fp, input);
		assertEquals(new BigDecimal(30.25), fp.totalCredits);
		assertEquals(new BigDecimal(0.00), fp.totalDebits);
		assertEquals(0, fp.corruptLines);
		
	}
	
	@Test
	public void testProcessRecord3() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		String input = "2000018dij29,30.25";
		processRecordMethod.invoke(fp, input);
		assertEquals(new BigDecimal(0.00), fp.totalCredits);
		assertEquals(new BigDecimal(0.00), fp.totalDebits);
		assertEquals(1, fp.corruptLines);
		
	}
	
	@Test
	public void testProcessInputFile() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		String input = "G:\\greaterbank\\files\\pending\\finance_customer_transactions-20171201060101.csv";
		processInputFileMethod.invoke(fp, input);
		assertEquals(8, fp.processedLines);
		
	}
	
	@Test
	public void testProcessInputFileForException() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		String input = "G:\\dummy\\files\\pending\\finance_customer_transactions-20171201060101.csv";
		try{
			processInputFileMethod.invoke(fp, input);
		}catch(Exception ex){
			assertTrue(ex instanceof IOException);
			assertEquals("IOException occurred G:\\dummy\\files\\pending\finance_customer_transactions-20171201060101.csv (The system cannot find the path specified)", ex.getMessage());
		}
		
	}
	
	@Test
	public void testwriteReport() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		String input = "testabc.txt";
		writeReportMethod.invoke(fp, input);
		File folder = new File(System.getenv("TRANSACTION_PROCESSING")+"\\"+"reports");
		assertEquals(1,folder.listFiles().length);
		File[] allFiles = folder.listFiles();
		assertTrue(allFiles[0].getName().contains("finance_customer_transactions_report"));
		try (FileReader fr = new FileReader(allFiles[0]);
				Scanner sc = new Scanner(fr)) {
					assertEquals("File Processed: testabc.txt", sc.nextLine());
					assertEquals("Total Accounts: 0", sc.nextLine());
					assertEquals("Total Credits: $0", sc.nextLine());
					assertEquals("Total Debits: $0", sc.nextLine());
					assertEquals("Skipped Transactions: 0", sc.nextLine());
					
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testMoveFile() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		File input = new File("G:\\test\\test.txt");
		File folder = new File(System.getenv("TRANSACTION_PROCESSING")+"\\"+"processed");
		final File[] files = folder.listFiles();
		for(File f: files) {
			f.delete();
		}
		moveFileMethod.invoke(fp, input);
		File[] allFiles = folder.listFiles();
		assertTrue(allFiles[0].getName().contains("test.txt"));
		File originalFolder = new File("G:\\test");
		final File[] originalFiles = originalFolder.listFiles();
		assertEquals(0, originalFiles.length);
	}
	
	@Test
	public void testMain(){
		FileProcessor fileProc = new FileProcessor();
		String[] args = new String[1];
		fileProc.main(args);
		assertEquals(10, fileProc.processedLines);
		assertEquals(4, fileProc.corruptLines);
		assertEquals(new BigDecimal(300.25), fileProc.totalCredits);
		assertEquals(new BigDecimal(20080.00), fileProc.totalDebits);
	}

}
